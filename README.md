# IGB Chrome Extension

## Objective
This extension is meant to extend bioinformatic websites to make use of the REST API endpoints made available by the Integrated Genome Browser (IGB). This can be useful in allowing e.g. importing data into IGB for visualization and analysis.

## Features
- Shows that the extension can make calls to IGB endpoints. Confirm by opening the extension's `background page` from within the Google Chrome [extensions page](chrome://extensions/) and viewing the console. The sample call made is executed in the extension's background script.
- Provides a user interface that can be accessed by clicking on the extension icon when on *bioviz.org* domains. This UI currently contains a button that allows users to manually check whether or not IGB is running. The extension UI can be made available on any number of websites.
- Injects a content script into the [genome dashboard](https://www.bioviz.org/genome-dashboard) that sends user search input to the extension, which then sends it back to the web app, where it is logged in the console. This demonstrates data transfer between the extension and a web app. Data transfer may be one-way or two-way and can be combined with requests to the IGB API.

## Installation
Add this extension to your Google Chrome browser

- Clone this repository.
- Open the Google Chrome extensions page at `chrome://extensions`.
- Enable developer mode.
- Click `load unpacked`
- Open the cloned folder.

## Configuration
Update the clone with your unique extension identifier

- Copy the extension `ID` string shown at `chrome://extensions`
- Replace the existing value for the `extensionId` constant in `sendSearch.js` with this value
- Reload the extension by clicking the circular arrow icon
