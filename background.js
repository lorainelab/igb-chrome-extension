// Configure the extension
chrome.runtime.onInstalled.addListener(() => {
  // Hit IGB endpoint on extension install
  fetch('http://127.0.0.1:7085/igbStatusCheck')
    .then(res => {
      console.log(res.text());
    })
    .catch(err => {
      console.log('IGB is not running');
    })
  // Allow a user to manually check whether IGB is running using the extension UI
  // Active for all bioviz.org domains
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [new chrome.declarativeContent.PageStateMatcher({
        pageUrl: {hostSuffix: 'bioviz.org'},
      })
      ],
          actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});

// Listen to messages from web apps (currently only genome dashboard)
chrome.runtime.onMessage.addListener( (message, sender, sendResponse) => {
  sender.tab ?
  sendResponse(message.userSearch) :
  console.log('Message not from web app');
});
